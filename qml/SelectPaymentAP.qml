import QtQuick 2.4
import QtQuick.Controls 1.2

BaseAP{
    id: baseAP
    property var press: '0'
    watermark: true
    topPanelColor: 'BLUE'
    mainMode: false
    property var phone_number: ''
    property var locker_name: ''
    property var selectedPayment: ''
    property int timer_value: 300
    property bool emoneyReady: false
    property bool bniYapReady: true
    property bool isExtending: false
    property var extendData: undefined


    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            press = '0';
            abc.counter = timer_value;
            my_timer.start();
            selectedPayment = '';
            slot_handler.start_get_cod_status();
            slot_handler.start_get_locker_name();
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop();
        }
    }

    Component.onCompleted: {
        root.start_get_cod_status_result.connect(cod_result);
        root.start_get_locker_name_result.connect(show_locker_name);
    }

    Component.onDestruction: {
        root.start_get_cod_status_result.disconnect(cod_result);
        root.start_get_locker_name_result.disconnect(show_locker_name);
    }

    function show_locker_name(text){
        if(text == ""){
            return
        }
        locker_name = text;
    }


/*
extendData = '{"id": "fc6852c09ba211e89941509a4ccea7f5", "mouth_id": "4028808359ff616b015a172842265752", "amount": 2000, "express_id": "fb4f280a9ba211e8be26509a4ccea7f5", "createTi
me": 1533798495000, "transactionType": "{\"express\": \"APXID-9M2VX\", \"pin_code\": \"E3S9VC\", \"result\": \"SUCCESS\", \"door_no\": 44, \"phone_number\": \"085710157057\", \"date\": \"2018-08-09
 22:08:13\"}", "paymentType": "E3S9VC"}'
*/

    function cod_result(result){
        console.log("cod_result is ", result)
        if(result=="enabled"){
            emoneyReady = true;
        }
    }

    BackButtonAP{
        id:back_button
        x:20
        y:20

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_timer.stop()
                my_stack_view.pop()
            }
        }
    }

    Rectangle{
        id: timer_set
        x:50
        y:50
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    Image {
        id: step_progress
        anchors.top: parent.top
        anchors.topMargin: 100
        anchors.horizontalCenter: parent.horizontalCenter
        source: "img/apservice/langkah/2.png"
    }

    ChannelButtonAP{
        id: emoney
        x: 205
        y: 284
        width: 200
        height: 200
        show_text:"MANDIRI EMONEY"
        show_image:"img/apservice/logo/emoney.png"
        paymentBase: 'card'
        buttonActive: emoneyReady
        colorTheme: 'BLUE' //'BLUE'/'GREEN'/\
        MouseArea{
            anchors.fill: parent
            enabled: parent.buttonActive
            onClicked: {
                if (press!='0') return;
                press = '1';
                selectedPayment = 'emoney';
                my_stack_view.push(confirm_payment_ap, {selectedPayment: selectedPayment,
                                       phone_number: phone_number, isExtending: isExtending,
                                   extendData: extendData, locker_name: locker_name});
            }
        }
    }

    ChannelButtonAP{
        id: yap
        x: 592
        y: 284
        width: 200
        height: 200
        show_text:"BNI YAP"
        show_image:"img/apservice/logo/yap.png"
        paymentBase: 'qr'
        buttonActive: bniYapReady
        colorTheme: 'BLUE' //'BLUE'/'GREEN'/
        MouseArea{
            anchors.fill: parent
            enabled: parent.buttonActive
            onClicked: {
                if (press!='0') return;
                press = '1';
                selectedPayment = 'bni-yap';
                my_stack_view.push(confirm_payment_ap, {selectedPayment: selectedPayment,
                                       phone_number: phone_number, isExtending: isExtending,
                                   extendData: extendData, locker_name: locker_name});
            }
        }
    }

    Notification{
        id: notif_text
        contentNotif: qsTr('Pastikan Nomor Telepon Anda adalah Benar.')
    }


}
