var values = []

function intersect_arrays(a, b) {
    var sorted_a = a.concat().sort();
    var sorted_b = b.concat().sort();
    var common = [];
    var a_i = 0;
    var b_i = 0;

    while (a_i < a.length
           && b_i < b.length)
    {
        if (sorted_a[a_i] === sorted_b[b_i]) {
            common.push(sorted_a[a_i]);
            a_i++;
            b_i++;
        }
        else if(sorted_a[a_i] < sorted_b[b_i]) {
            a_i++;
        }
        else {
            b_i++;
        }
    }
    return common;
}

function currency(a, c){
    if(a != undefined){
        var newstrA=""
        newstrA = a.replace(/(\d{1,3})(?=(?:\d{3})+(?!\d))/g,'$1.')
        return newstrA
    }else{
        return c + a
    }
}

function selectedValues() {
    return values
}

function isSelected(value) {
    return (values.indexOf(value) != -1)
}

function addValue(value) {
    if (values.indexOf(value) != -1)
        return
    values.push(value)
}

function removeValue(value) {
    var index = values.indexOf(value)

    if (index == -1)
        return
    values.splice(index, 1)
}

function insert_dot(str){
    var newstr=""
    newstr = str.replace(/(\d{1,3})(?=(?:\d{3})+(?!\d))/g,'$1.')
    return newstr
}

function insert_space(str){
    return str.replace(/(\d{4})/g, '$1 ').replace(/(^\s+|\s+$)/,'')
}


