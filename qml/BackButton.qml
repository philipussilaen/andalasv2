import QtQuick 2.4
import QtQuick.Controls 1.2



Rectangle{

    width:60
    height:60
    color:"transparent"

    property var show_text: qsTr("BACK")
    property var show_source: "img/button/7.png"
    property var show_img: "img/button/white_left_arrow_new.png"
    property bool vis_text : false
    property bool vis_base : false

    Image{
        id: foreground
        visible: vis_base
        width:120 
        height:50
        source:show_source
    }

    Image{
        id: back_arrow
        anchors.fill: parent
        anchors.centerIn: parent
        fillMode: Image.Stretch
        source: show_img
    }


    Text{
        visible : vis_text
        x: 25
        y: 31
        font.family:"Microsoft YaHei"
        text:show_text
        color:"#ff0000"
        font.pixelSize:24
        anchors.centerIn: parent;
    }
}

