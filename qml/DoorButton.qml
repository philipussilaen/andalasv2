import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    property var show_text:""
    property var show_image:""
    property var show_color:"transparent"

    width:279
    height:64
    color:"transparent"

    Image{
        x:0
        y:0
        width:279
        height:64
        source:show_image
    }

    Rectangle{
        width:279
        height:64
        color:show_color

        Text{
            text:show_text
            color:"red"
            font.family:"Microsoft YaHei"
            font.pixelSize:24
            anchors.centerIn: parent;
        }
    }
}
