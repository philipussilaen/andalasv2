import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: other_payment_confirmation_page

    property var balance:"0"
    property var total_amount:"0"
    property int timer_value: 60
    property var press:"0"
    width: 1024
    height: 768

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            abc.counter = timer_value
            my_timer.restart()
            press = "0"
            ok_button.enabled = true
            back_button.enabled = true
        }

        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }
        Text{
            id:show_time
            x:60
            y:50
            font.family:"Microsoft YaHei"
            color:"#f8f400"
            textFormat: Text.PlainText
            font.pointSize:33
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                show_time.text = abc.counter
                abc.counter -= 1
                if(abc .counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

     FullWidthReminderText{
        id:text
        y:140
        remind_text:qsTr("Payment confirmation")
        remind_text_size:"35"
     }

    DoorButton{
        id:back_button
        y:604
        x:232
        show_text:qsTr("Cancel")
        show_image:"img/door/1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 1) return true }))
            }
            onEntered:{
                back_button.show_image = "img/door/2.png"
            }
            onExited:{
                back_button.show_image = "img/door/1.png"
            }
        }
    }

    DoorButton{
        id:ok_button
        y:604
        x:524
        show_text:qsTr("Confirm")
        show_image:"img/door/1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                ok_button.enabled = false
                back_button.enabled = false
                loading.open()
                slot_handler.start_payment_by_card()
            }
            onEntered:{
                ok_button.show_image = "img/door/2.png"
            }
            onExited:{
                ok_button.show_image = "img/door/1.png"
            }
        }
    }

    Row{
        x:270
        y:250
        spacing:10
        Column{
            spacing:20
            Text {
                text: qsTr("Balance")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 30
            }
            Text {
                text: qsTr("Total Amount")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 30
            }
        }

        Row{
            spacing:10
            Column{
                spacing:20
                Text {
                    text: qsTr(":")
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
                Text {
                    text: qsTr(":")
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
            }
            Column{
                spacing:20
                Text {
                    text: balance
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
                Text {
                    text: total_amount
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
            }
        }
    }

    FullWidthReminderText{
        y:415
        remind_text:qsTr("Please do not remove your pre-paid card.")
        remind_text_size:"30"
    }

    FullWidthReminderText{
        y:500
        remind_text:qsTr("By clicking confirm button you agree to the Terms and Conditions.")
        remind_text_size:"25"
    }

    LoadingView{
        id:loading
    }

    Component.onCompleted: {
        root.start_payment_by_card_result.connect(handle_text)
    }

    Component.onDestruction: {
        root.start_payment_by_card_result.disconnect(handle_text)
    }

    function handle_text(result){
        if(result == "ERROR"){
            my_stack_view.push(other_payment_failed_page)
            return
        }
        var obj = JSON.parse(result)
        if(obj.length <= 0){
            my_stack_view.push(other_payment_failed_page)
        }else{
            my_stack_view.push(other_payment_successful_page, {payment_result:result, amount:total_amount})
        }
    }
}
