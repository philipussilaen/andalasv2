import QtQuick 2.4
import QtQuick.Controls 1.2


Rectangle{
    id: rectangle1

    width:100
    height:50
    color:"transparent"

    property var show_text:""
    property var show_image:""
    property var show_x
    property var show_source:"img/05/button.png"

    Image{
        width:100
        height:50
        source:show_source
    }

    Text{
        text:show_text
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        color:"red"
        font.family:"Microsoft YaHei"
        font.pixelSize:30
    }

}
