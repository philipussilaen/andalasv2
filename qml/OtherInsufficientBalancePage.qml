import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: other_insufficient_balance_page

    property var balance:100
    property var total_amount:100
    property int timer_value: 60
    width: 1024
    height: 768

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            abc.counter = timer_value
            my_timer.restart()
        }

        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }


    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }
        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc .counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }


    FullWidthReminderText{
        id:text
        y:140
        remind_text:qsTr("Insufficient balance")
        remind_text_size:"35"
    }

    DoorButton{
        id:back_button
        y:604
        x:370
        show_text:qsTr("OK")
        show_image:"img/door/1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 1) return true }))
            }
            onEntered:{
                back_button.show_image = "img/door/2.png"
            }
            onExited:{
                back_button.show_image = "img/door/1.png"
            }
        }
    }


    Row{
        x:270
        y:250
        spacing:10
        Column{
            spacing:20
            Text {
                text: qsTr("Balance")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 30
            }

            Text {
                text: qsTr("Total Amount")
                font.family:"Microsoft YaHei"
                color:"#ffffff"
                font.pixelSize: 30
            }
        }
        Row{
            spacing:10
            Column{
                spacing:20
                Text {
                    text: qsTr(":")
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
                Text {
                    text: qsTr(":")
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
            }
            Column{
                spacing:20
                Text {
                    text: balance
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
                Text {
                    text: total_amount
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
            }
        }
    }

    FullWidthReminderText{
        y:415
        remind_text:qsTr("Please top up your prepaid card.")
        remind_text_size:"30"
    }

}
