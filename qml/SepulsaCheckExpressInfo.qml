import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: sepulsa_check_express_info
    width: 1024
    height: 768
    property int timer_value: 30
    property var trx_id:"PLSGHLKJD-UIJK"
    property var detail_result: '{"phone": "082122457037", "product_name": "Telkomsel PopBox 20K","nominal": "20000","status": "success","status_text": "Transaksi Sukses "}'
    property var phone_no:"-"
    property var product_name:"-"
    property var nominal:"-"
    property var trx_status:"-"

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            abc.counter = timer_value
            my_timer.restart()
            loadingGif.open()
            parse_status(detail_result)
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Component.onCompleted: {
    }

    Component.onDestruction: {
    }

    function insert_flg(str){
        var newstr=""
        newstr = str.replace(/(\d{1,3})(?=(?:\d{3})+(?!\d))/g,'$1.')
        return newstr
    }

    function parse_status(xdata){
        if(xdata == ""){
            return
        }

        var obj = JSON.parse(xdata)
        phone_no = obj.phone
        product_name = obj.product_name
        nominal = obj.nominal
        trx_status = obj.status_text
        if(phone_no != "-" && product_name != "-" && nominal != "-" && trx_status != "-"){
            loadingGif.close()
        }
    }

    Rectangle{
        x: 10
        y: 10
        width: 20
        height: 20
        color: "transparent"

        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                main_timer_text.text = abc.counter
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }

        Text {
            id:main_timer_text
            x: -10
            y: 717
            width: 40
            height: 40
            font.family:"Microsoft YaHei"
            color:"#fff000"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            textFormat: Text.PlainText
            font.pointSize:15
            wrapMode: Text.WordWrap
        }
    }

    Rectangle{
        x: 0
        y:140
        width: parent.width
        height: 60
        color: "darkred"
        Text{
            id: title_text
            height: 80
            anchors.fill: parent
            text: qsTr("Transaction Details")
            verticalAlignment: Text.AlignVCenter
            font.family:"Microsoft YaHei"
            horizontalAlignment: Text.AlignHCenter
            color:"#FFFFFF"
            font.pixelSize:40
        }
    }

    BackButton{
        id:back_button
        x:20
        y:20
        show_text:qsTr("return")

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_timer.stop()
                my_stack_view.pop()
            }
            onEntered:{
                back_button.show_source = "img/bottondown/down.2.png"
            }
            onExited:{
                back_button.show_source = "img/button/7.png"
            }
        }
    }

    Rectangle{
        id: foreground_info_text
        x: 137
        y: 216
        width: 750
        height: 360
        radius: 10
        opacity: 0.75
        color: "white"
    }

    Row{
        x:162
        y:230
        width: 700
        height: 300
        spacing:15
        Column{
            spacing:15
            Text {
                text: qsTr("Transaction No.")
                font.family:"Microsoft YaHei"
                color:"darkred"
                font.pixelSize: 30
            }
            Text {
                text: qsTr("Phone Number")
                font.family:"Microsoft YaHei"
                color:"darkred"
                font.pixelSize: 30
            }
            Text {
                text: qsTr("Product Name")
                font.family:"Microsoft YaHei"
                color:"darkred"
                font.pixelSize: 30
            }
            Text {
                text: qsTr("Amount")
                font.family:"Microsoft YaHei"
                color:"darkred"
                font.pixelSize: 30
            }
            Text {
                text: qsTr("Status")
                font.family:"Microsoft YaHei"
                color:"darkred"
                font.pixelSize: 30
            }
        }
        Row{
            width: 400
            spacing:10
            Column{
                spacing:15
                Text {
                    text: ":"
                    font.family:"Microsoft YaHei"
                    color:"darkred"
                    font.pixelSize: 30
                }
                Text {
                    text: ":"
                    font.family:"Microsoft YaHei"
                    color:"darkred"
                    font.pixelSize: 30
                }
                Text {
                    text: ":"
                    font.family:"Microsoft YaHei"
                    color:"darkred"
                    font.pixelSize: 30
                }
                Text {
                    text: ":"
                    font.family:"Microsoft YaHei"
                    color:"darkred"
                    font.pixelSize: 30
                }
                Text {
                    text: ":"
                    font.family:"Microsoft YaHei"
                    color:"darkred"
                    font.pixelSize: 30
                }
            }
            Column{
                spacing:15
                Text {
                    id: trx_no_text
                    text: trx_id
                    font.family:"Microsoft YaHei"
                    color:"darkred"
                    font.pixelSize: 30
                    font.capitalization: Font.Capitalize
                }
                Text {
                    id: phone_no_txt
                    text: phone_no
                    font.family:"Microsoft YaHei"
                    color:"darkred"
                    font.pixelSize: 30
                }
                Text {
                    id: product_name_txt
                    text: product_name
                    font.family:"Microsoft YaHei"
                    color:"darkred"
                    font.pixelSize: 30
                    font.capitalization: Font.Capitalize
                }
                Text {
                    id: amount_txt
                    text: insert_flg(nominal)
                    font.family:"Microsoft YaHei"
                    color:"darkred"
                    font.pixelSize: 30
                }
                Text {
                    id: status_txt
                    width: 440
                    height: 120
                    text: trx_status
                    font.family:"Microsoft YaHei"
                    color:"darkred"
                    font.pixelSize: 30
                    font.capitalization: Font.Capitalize
                    wrapMode: Text.WordWrap
                }
            }
        }
    }

    DoorButton{
        id:check_again_button
        y:605
        x:211
        show_text:qsTr("Check Another")
        show_image:"img/door/1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_stack_view.pop()
            }
            onEntered:{
                check_again_button.show_image = "img/door/2.png"
            }
            onExited:{
                check_again_button.show_image = "img/door/1.png"
            }
        }
    }

    DoorButton{
        id:ok_button
        y:605
        x:512
        show_text:qsTr("OK")
        show_image:"img/door/1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
            }
            onEntered:{
                ok_button.show_image = "img/door/2.png"
            }
            onExited:{
                ok_button.show_image = "img/door/1.png"
            }
        }
    }

    Text {
        id: help_text
        x: 8
        y: 693
        width: 1000
        height: 50
        font.family:"Microsoft YaHei"
        color:"#ffffff"
        font.pixelSize: 25
        text: qsTr("For further assistance, Please feel free to contact us at 021-29022537/38.")
        font.italic: true
        wrapMode: Text.WordWrap
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }

    LoadingView{
        id:loadingGif
    }
}
