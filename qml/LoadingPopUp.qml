import QtQuick 2.4

Rectangle{
    id: main_rectangle
    color: "transparent"
    property alias popupText: notif_text.text
    width: 1024
    height: 768
    visible: false

    Rectangle{
        id: opacity_rec
        anchors.fill: parent
        color: "#828282"
    }

    Text{
        id: notif_text
        x: 86
        y: 179
        width: 450
        height: 100
        font.family: 'Microfot YaHei'
        font.pixelSize: 30
        color: 'white'
        text: qsTr("Memproses...")
        anchors.verticalCenterOffset: 100
        wrapMode: Text.WordWrap
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
    }

    AnimatedImage{
        id: notif_img
        width: 150
        height: 150
        anchors.verticalCenterOffset: 0
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        source: 'img/apservice/spin.gif'
    }


    function open(){
        main_rectangle.visible = true;
    }

    function close(){
        main_rectangle.visible = false;
    }

}





