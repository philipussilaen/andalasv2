import QtQuick 2.4

Rectangle {
    id: main_rectangle
    width: 250
    height: 75
    property var buttonColor: 'BLUE' //'BLUE'/'GREEN'
    property bool modeReverse: false
    property alias buttonText: button_text.text
    color: define_color('background')
    radius: 20
    border.width: (modeReverse==true) ? 3 : 0
    border.color: button_text.color
    Text {
        id: button_text
        anchors.fill: parent
//        color: (modeReverse==false) ? 'white' : (buttonColor=='BLUE') ? "#009BE1" : "#65B82E"
        text: qsTr("OK")
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        font.family: 'Microsoft YaHei'
        font.bold: true
        font.pixelSize: 20
        color: define_color('text')
    }

    function define_color(t){
        if (t=='background'){
            if (modeReverse==false){
                if (buttonColor=='BLUE'){
                    return "#009BE1";
                } else if (buttonColor=='GREEN'){
                    return "#65B82E";
                } else {
                    return "gray";
                }
            } else {
                return 'white';
            }
        } else {
            if (modeReverse==true){
                if (buttonColor=='BLUE'){
                    return "#009BE1";
                } else if (buttonColor=='GREEN'){
                    return "#65B82E";
                } else {
                    return "gray";
                }
            } else {
                return 'white';
            }
        }
    }



}
