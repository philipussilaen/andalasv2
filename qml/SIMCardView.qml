import QtQuick 2.0

Rectangle{
    id: rectangle
    property var init_price: "100.000"
    property var phone_no: "08121234567"
    property var new_price: "97.500"
    property var operator: "telkomsel"
    property var sim_type: "Regular"
    property var product_desc: "Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum"
    width: 250
    height: 150

    Image {
        id: base_image
        anchors.fill: parent
        source: "img/item/sim_card_plain.png"
        fillMode: Image.Stretch
    }

    function get_logo(o){
        switch(o){
        case 'telkomsel':
            return "img/sepulsa/logo/telkomsel.png"
        case 'indosat':
            return "img/sepulsa/logo/indosat.png"
        case 'xl':
            return "img/sepulsa/logo/xl.png"
        case 'bolt':
            return "img/sepulsa/logo/bolt.png"
        default:
            return "img/sepulsa/logo/simpati.png"
        }
    }

    Image {
        source: get_logo(operator)
//        source: "img/sepulsa/logo/indosat.png"
        width: 100
        height: 50
        fillMode: Image.PreserveAspectFit
    }

    Text {
        id: prod_type_text
        color: "#403a3a"
        text: sim_type
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignRight
        anchors.top: parent.top
        anchors.topMargin: 20
        anchors.right: parent.right
        anchors.rightMargin: 20
        font.family: 'Microsoft YaHei'
        font.pixelSize: 12
        font.bold: true
    }

    Text {
        id: phone_no_text
        text: phone_no
        anchors.top: parent.top
        anchors.topMargin: 60
        anchors.left: parent.left
        anchors.leftMargin: 10
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignLeft
        font.family: 'Microsoft YaHei'
        font.pixelSize: 15
        font.bold: true
        color: 'black'
    }


    Text {
        id: init_price_text
        text: init_price
        anchors.top: parent.top
        anchors.topMargin: 75
        anchors.left: parent.left
        anchors.leftMargin: 10
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignLeft
        font.family: 'Microsoft YaHei'
        font.pixelSize: 10
        font.bold: false
        font.strikeout: true
        font.italic: true
        color: 'darkred'
    }

    Text {
        id: final_price_text
        text: new_price
        anchors.top: parent.top
        anchors.topMargin: 90
        anchors.left: parent.left
        anchors.leftMargin: 50
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignLeft
        font.family: 'Microsoft YaHei'
        font.pixelSize: 15
        font.bold: true
        color: 'darkred'
    }

    Text {
        id: prod_desc_text
        text: product_desc
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 5
        width: parent.width - 20
        wrapMode: Text.WordWrap
        anchors.left: parent.left
        anchors.leftMargin: 10
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignLeft
        font.family: 'Microsoft YaHei'
        font.pixelSize: 9
        font.bold: false
        font.italic: true
        color: 'gray'
    }


}
