import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: popsend_topup_payment_success
    width: 1024
    height: 768
    property int timer_value: 60
    property int timer_thanks: 10
    property var last_balance:""
    property var card:""
    property var show_date:""
    property var locker:""
    property var terminal_id:""
    property var payment_result:'{"show_date": 1483511917000, "locker": "Demo Locker", "last_balance": "995000", "card_no": "1234567887654321", "terminal_id": "01234567"}'
    property var amount
    property var member_data
    property var cust_name:""
    property var cust_phone:""
    property var cust_email:""
    property var cust_lastbalance:""
    property var payment_type:"emoney"
    property var new_amount:"-"
    property var new_denom:"-"
    property var denom

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            abc.counter = timer_value
            my_timer.restart()
            define_member(member_data)
            show_payment_result(payment_result)
            thank_you_notif.close()
            //Topup from amount which will be added with extra amount as discount if available.
            slot_handler.start_popsend_topup(amount)
        }

        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
            show_timer_thanks.stop()
        }
    }

    Component.onCompleted: {
        root.start_popsend_topup_result.connect(handle_text)
    }

    Component.onDestruction: {
        root.start_popsend_topup_result.disconnect(handle_text)
    }

    function define_member(member_data){
        var info = JSON.parse(member_data)
        cust_name = info.member_name
        cust_phone = info.phone
        cust_email = info.email
    }

    function show_payment_result(payment_result){
        console.log("settlement_result : " + payment_result)
        if(payment_result == ""){
            return
        }

        var obj = JSON.parse(payment_result)
        terminal_id = obj.terminal_id
        locker = obj.locker
        last_balance = insert_flg(obj.last_balance + "")
        card = obj.card_no
        new_amount = insert_flg(amount)
        new_denom = insert_flg(denom)

        var now_date = new Date(obj.show_date)
        var time_Y = add_zero(now_date.getFullYear());
        var mouth = (now_date.getMonth()+1 < 10 ? +(now_date.getMonth()+1) : now_date.getMonth()+1)
        switch (mouth){
            case 1: var time_M = "January"
                break;
            case 2: var time_M = "February"
                break;
            case 3: var time_M = "March"
                break;
            case 4: var time_M = "April"
                break;
            case 5: var time_M = "May"
                break;
            case 6: var time_M = "June"
                break;
            case 7: var time_M = "July"
                break;
            case 8: var time_M = "August"
                break ;
            case 9: var time_M = "September"
                break;
            case 10: var time_M = "October"
                break;
            case 11: var time_M = "November"
                break;
            case 12: var time_M = "December"
                break;
        }

        var time_D = add_zero(now_date.getDate());
        var time_h = add_zero(now_date.getHours());
        var time_m = add_zero(now_date.getMinutes());
        var time_s = add_zero(now_date.getSeconds());
        show_date = time_D+" "+time_M+" "+time_Y+" "+time_h+":"+time_m+":"+time_s
    }

    function add_zero(temp){
        if(temp<10) return "0"+temp;
        else return temp;
    }

    function change_mouth(mouth){
        switch (mouth){
            case "1": print("January")
                break;
            case "2": print("February")
                break;
            case "3": print("March")
                break;
            case "4": print("April")
                break;
            case "5": print("May")
                break;
            case "6": print("June")
                break;
            case "7": print("July")
                break;
            case "8": print("August")
                break ;
            case "9": print("September")
                break;
            case "10": print("October")
                break;
            case "11": print("November")
                break;
            case "12": print("December")
                break;
        }
    }

    function insert_flg(str){
        var newstr=""
        newstr = str.replace(/(\d{1,3})(?=(?:\d{3})+(?!\d))/g,'$1.')
        return newstr
    }

    function handle_text(result){
        console.log('start_popsend_topup_result :' + result)
        if(result != "NOT FOUND"){
            cust_lastbalance = result
        }else{
            show_timer_thanks.restart()
        }
    }

    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    main_page.enabled = false
                    ok_button_main_page.enabled = false
                    show_timer_thanks.start()
                    thank_you_notif.open()
                }
            }
        }
    }

    Rectangle{
        id: main_page
        FullWidthReminderText{
           id:text
           y:140
           remind_text:qsTr("Order Details")
           remind_text_size:"35"
        }

        FullWidthReminderText{
           id:text1
           x: 0
           y:566
           remind_text:qsTr("The sms notification will be sent to you for this credit purchase")
           remind_text_size:"30"
        }

        DoorButton{
            id:ok_button_main_page
            y:671
            x:373
            show_text:qsTr("OK")
            show_image:"img/door/1.png"

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_timer.stop()
                    main_page.enabled = false
                    ok_button_main_page.enabled = false
                    show_timer_thanks.start()
                    thank_you_notif.open()
                }
                onEntered:{
                    ok_button_main_page.show_image = "img/door/2.png"
                }
                onExited:{
                    ok_button_main_page.show_image = "img/door/1.png"
                }
            }
        }

        Row{
            x:223
            y:200
            spacing:30
            Column{
                spacing:10
                Text {
                    text: qsTr("Card No.")
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
                Text {
                    text: qsTr("Last Balance")
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
                Text {
                    text: qsTr("Product Item")
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
                Text {
                    text: qsTr("Amount")
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
                Text {
                    text: qsTr("Date")
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
                Text {
                    text: qsTr("Terminal ID")
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
                Text {
                    text: qsTr("Locker Name")
                    font.family:"Microsoft YaHei"
                    color:"#ffffff"
                    font.pixelSize: 30
                }
            }
            Row{
                spacing:10
                Column{
                    spacing:10
                    Text {
                        text: ":"
                        font.family:"Microsoft YaHei"
                        color:"#ffffff"
                        font.pixelSize: 30
                    }
                    Text {
                        text: ":"
                        font.family:"Microsoft YaHei"
                        color:"#ffffff"
                        font.pixelSize: 30
                    }
                    Text {
                        text: ":"
                        font.family:"Microsoft YaHei"
                        color:"#ffffff"
                        font.pixelSize: 30
                    }
                    Text {
                        text: ":"
                        font.family:"Microsoft YaHei"
                        color:"#ffffff"
                        font.pixelSize: 30
                    }
                    Text {
                        text: ":"
                        font.family:"Microsoft YaHei"
                        color:"#ffffff"
                        font.pixelSize: 30
                    }
                    Text {
                        text: ":"
                        font.family:"Microsoft YaHei"
                        color:"#ffffff"
                        font.pixelSize: 30
                    }
                    Text {
                        text: ":"
                        font.family:"Microsoft YaHei"
                        color:"#ffffff"
                        font.pixelSize: 30
                    }
                }
                Column{
                    spacing:10
                    Text {
                        text: card
                        font.family:"Microsoft YaHei"
                        color:"#ffffff"
                        font.pixelSize: 30
                    }
                    Text {
                        text: last_balance
                        font.family:"Microsoft YaHei"
                        color:"#ffffff"
                        font.pixelSize: 30
                    }
                    Text {
                        text: 'Top Up PopSend' + ' - ' + new_denom
                        font.family:"Microsoft YaHei"
                        color:"#ffffff"
                        font.pixelSize: 30
                        font.capitalization: Font.Capitalize
                    }
                    Text {
                        text: new_amount
                        font.family:"Microsoft YaHei"
                        color:"#ffffff"
                        font.pixelSize: 30
                    }
                    Text {
                        text: show_date
                        font.family:"Microsoft YaHei"
                        color:"#ffffff"
                        font.pixelSize: 30
                    }
                    Text {
                        text: terminal_id.toString()
                        font.family:"Microsoft YaHei"
                        color:"#ffffff"
                        font.pixelSize: 30
                    }
                    Text {
                        text: locker.toString()
                        font.family:"Microsoft YaHei"
                        color:"#ffffff"
                        font.pixelSize: 30
                        font.capitalization: Font.Capitalize
                    }
                }
            }
        }

        FullWidthReminderText {
            id: text2
            x: 0
            y: 612
            remind_text: qsTr("Please do not forget to take your prepaid card")
            remind_text_size: "30"
        }
    }

    FloatingWindow{
        id:thank_you_notif
        //visible:true

        Text {
            id: text_thank_you_notif_1
            x:192
            y:233
            width: 650
            height: 50
            text: "Hi, " + cust_name.toString()
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            color:"#ab312e"
            textFormat: Text.PlainText
            font.pointSize:25
            wrapMode: Text.WordWrap
        }

        Text {
            id: text_thank_you_notif_2
            x:192
            y:281
            width: 650
            height: 50
            text: qsTr("Your current PopSend balance is ") + insert_flg(cust_lastbalance.toString())
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            color:"#ab312e"
            textFormat: Text.PlainText
            font.pointSize:20
            wrapMode: Text.WordWrap
        }

        Text {
            id: text_thank_you_notif_3
            x:290
            y:344
            width: 600
            height: 180
            text: qsTr("Thank you for purchasing Your PopSend balance with PopBox Locker.")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            color:"#ab312e"
            textFormat: Text.PlainText
            font.pointSize:25
            wrapMode: Text.WordWrap
        }

        Rectangle{
            QtObject{
                id:time_thanks
                property int counter
                Component.onCompleted:{
                    time_thanks.counter = timer_thanks
                }
            }
            Row{
                x:848
                y:502
                spacing:5
                Text {
                    id:show_time_thanks
                    width: 50
                    font.family:"Microsoft YaHei"
                    color:"red"
                    textFormat: Text.PlainText
                    font.pointSize:25
                    wrapMode: Text.WordWrap
                }
            }

            Timer{
                id:show_timer_thanks
                interval:1000
                repeat:true
                running:false
                triggeredOnStart:true
                onTriggered:{
                    show_time_thanks.text = time_thanks.counter
                    time_thanks.counter -= 1
                    if(time_thanks.counter == 7){
                        console.log('settle topup popsend : ' + cust_phone + '-' + "topup_popsend" + '-' + amount + '-' + payment_type)
                        slot_handler.start_push_data_settlement(cust_phone + '-' + "topup_popsend" + '-' + amount + '-' + payment_type)
                    }
                    if(time_thanks.counter < 0){
                        show_timer_thanks.stop()
                        my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                    }
                }
            }
        }
    }
}
