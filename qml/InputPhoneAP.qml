import QtQuick 2.4
import QtQuick.Controls 1.2

BaseAP{
    id: baseAP
    property var press: '0'
    watermark: true
    topPanelColor: 'BLUE'
    mainMode: false
    property variant check_number: ['0814','0815', '0816', '0855', '0856', '0857', '0858',
        '0811', '0812', '0813', '0821', '0822', '0823', '0851', '0852', '0853',
        '0817', '0818', '0819', '0859', '0877', '0878', '0879',
        '0831', '0832', '0838', '9991', '9992', '9993', '9994', '9995', '9988',
        '0881', '0882', '0883', '0884', '0885', '0886', '0887', '0888', '0889',
        '0895', '0896', '0897', '0898', '0899']
    property var phone_number: ''
    property int timer_value: 300
    property int count: 0

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            press = '0';
            phone_number = '';
            abc.counter = timer_value;
            my_timer.start();
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop();
        }
    }

    Component.onCompleted: {
    }

    Component.onDestruction: {

    }

    BackButtonAP{
        id:back_button
        x:20
        y:20

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_timer.stop()
                my_stack_view.pop()
            }
        }
    }

    Rectangle{
        id: timer_set
        x:50
        y:50
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    Image {
        id: step_progress
        anchors.top: parent.top
        anchors.topMargin: 100
        anchors.horizontalCenter: parent.horizontalCenter
        source: "img/apservice/langkah/1.png"
    }

    Text {
        id: main_command_text
        width: 500
        height: 100
        color: "#323232"
        text: qsTr("Masukkan No HP")
        anchors.left: parent.left
        anchors.leftMargin: 50
        anchors.top: parent.top
        anchors.topMargin: 200
        font.bold: true
        font.family: 'Microsoft YaHei'
        font.pixelSize: 50

    }

    Rectangle{
        id: phone_number_input
        width:400
        height:60
        color:"transparent"
        radius: 10
        anchors.top: parent.top
        anchors.topMargin: 280
        anchors.left: parent.left
        anchors.leftMargin: 50
        border.color: 'gray'
        border.width: 2
        Image {
            id: icon_phone
            scale: 1.2
            anchors.left: parent.left
            anchors.leftMargin: 20
            anchors.verticalCenter: parent.verticalCenter
            source: (phone_number=='')  ? 'img/apservice/icon/hp_grey.png' : 'img/apservice/icon/hp_biru.png'
        }

        TextEdit{
            id:input_phone
            text:phone_number
            anchors.topMargin: 7
            anchors.leftMargin: 60
            anchors.fill: parent
            horizontalAlignment: Text.AlignLeft
            font.family:"Microsoft YaHei"
            color:"#323232"
            font.pixelSize:35
            font.bold: true

        }
}

    Rectangle{
        id: base_ground_keyboard
        color: 'black'
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        opacity: 0.65
        width: parent.width
        height: 400
    }

    NumKeyboard{
        id:number_keyboard
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 20
        anchors.horizontalCenter: parent.horizontalCenter
        functionPad: false
        enabled: !notif_text.visible

        Component.onCompleted: {
            number_keyboard.letter_button_clicked.connect(show_validate_code);
            number_keyboard.function_button_clicked.connect(function_button_action);
        }

        NumButton{
            id: reset_button
            x: 0
            y: 270
            defaultColor: 'orange'
            img_source: ''
            show_text: qsTr('HAPUS')
            textColor: 'white'
            textSize: 20
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    count--
                    phone_number = phone_number.substring(0,phone_number.length-1);
                }
            }
        }

        NumButton{
            id: find_button
            x: 180
            y: 270
            defaultColor: 'green'
            img_source: ''
            show_text: qsTr('SELESAI')
            textColor: 'white'
            textSize: 20
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    if (press!='0') return
                    press = '1'
                    if(phone_number != ""){
                        count=0;
                        if(check_number.indexOf(phone_number.substring(0,4)) > -1 && phone_number.length > 9){
                            my_stack_view.push(select_payment_ap, {phone_number: phone_number})
                        }else{
                            notif_text.open();
                        }
                    }else{
                        notif_text.open();
                    }
                }
            }
        }

        function function_button_action(str){
        }

        function show_validate_code(str){
            if (str != "" && count < 15){
                count++;
            }
            if (count > 15){
                str="";
            }
            press = '0';
            phone_number += str;
            abc.counter = timer_value;
            my_timer.restart()
        }
    }

    Notification{
        id: notif_text
        contentNotif: qsTr('Pastikan Nomor Telepon Anda adalah Benar.')
        successMode: false;

    }


}
