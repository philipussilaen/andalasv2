import QtQuick 2.4
import QtQuick.Controls 1.2
import QtWebKit 3.0
import QtWebKit.experimental 1.0
//import QtTest 1.0

Background{
    id: background
    img_vis: true
    width: 1024
    height: 768
    property var _url : 'http://beta.popbox.asia/locker/maps/id/'
    property int timer_value: 120
    property var selected_locker: ""
    property var press: "0"
    property var status_loading: "null"
    property int attempt: 0
    property bool readyStatus: false
    property var links : {"bogor": _url + "bogor",
                          "depok": _url + "depok",
                          "bekasi": _url + "bekasi",
                          "tangerang": _url + "tangerang",
                          "jak_sel": _url + "jakarta%20selatan",
                          "jak_bar": _url + "jakarta%20barat",
                          "jak_ut": _url + "jakarta%20utara",
                          "jak_tim": _url + "jakarta%20timur",
                          "jak_pus": _url + "jakarta%20"}

    property var init_url: links.jak_bar

    Stack.onStatusChanged:{
       if(Stack.status==Stack.Activating){           
           reload_url(init_url)
           press = "0"
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }

    }

    Component.onCompleted:{
    }

    Component.onDestruction:{
    }


    Rectangle{
        width:10
        height:10
        y:10
        color:"transparent"
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }
        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                countShow_test.text = abc.counter
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    function select_locker(){
        webview.experimental.evaluateJavaScript(
        "(function(){return document.getElementById('selected_locker').innerText})()",
        function(result){selected_locker = result});
    }

    function reload_url(__url){
        abc.counter = timer_value
        my_timer.restart()
        loadingGif.open();
        webview.url = Qt.resolvedUrl(__url);
        console.log("webview url : ", webview.url)
    }

    ScrollView {
        id: scroll_view
        width: 1024
        height: 768
        WebView {
            id: webview
            anchors.fill: parent
            experimental.userAgent:"Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36  (KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36"
            onLoadingChanged: {
                if(loadRequest.status==WebView.LoadSucceededStatus){
                    readyStatus = true
                    loadingGif.close()
                    status_loading = "finished";
                } else if(loadRequest.status==WebView.LoadFailedStatus){
                    status_loading = "failed";
                }
            }
        }
    }


    OverTimeButton{
        id: check_button
        x: 373
        y: 675
        show_text: qsTr("Select This Locker")
        show_x:15
        global_vis: readyStatus

        MouseArea {
            anchors.fill: check_button
            onClicked: {
                attempt += 1
                abc.counter = timer_value
                my_timer.restart()
                if(status_loading=="finished"){
                    select_locker()
                    if(attempt%2==0){
                        console.log("selected_locker : ", selected_locker)
                        attempt = 0
                    }
                } else {
                    console.log("sorry, page load status is : ", status_loading)
                }
            }
        }
    }


    LoadingView {id: loadingGif}

    BackButton{
        id:back_button
        x:20
        y:background.height/2
        show_text:qsTr("Depok")

        MouseArea {
            anchors.fill: parent
            onClicked: {
//                my_stack_view.pop()
                reload_url(links.depok)
            }
        }
    }

    BackButton{
        id:back_button1
        x:20
        y:background.height/3
        show_text:qsTr("Bogor")

        MouseArea {
            anchors.fill: parent
            onClicked: {
//                my_stack_view.pop()
                reload_url(links.bogor)
            }
        }
    }

    BackButton{
        id:back_button2
        x:20
        y:background.height/4
        show_text:qsTr("Tangerang")

        MouseArea {
            anchors.fill: parent
            onClicked: {
//                my_stack_view.pop()
                reload_url(links.tangerang)
            }
        }
    }

    Text{
        id:countShow_test
        x:10
        y:738
        color:"#FFFF00"
        font.family:"Microsoft YaHei"
        font.pixelSize:16
    }
}

