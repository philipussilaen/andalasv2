import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    id:parent_rec
    width: 375
    height: 150
    color:"transparent"

    property var lock_name: "Grand Slipi Tower"
    property var lock_addr_1: "Jl. S.Parman Kav.22-24, Slipi, Palmerah - Jakarta Barat 11480"
    property var lock_addr_2: "Lobby, belakang Apotik Century"
    property var lock_building: "Office"
    property var lock_operational: "Mon - Sun (09.00 AM - 10.00 PM)"
    property variant green_base: ["Office", "Shopping Mall"]
    property variant chocolate_base: ["Apartment/Residential", "Railway Station", "Cafe"]
    property variant red_base: ["Super/Mini Market", "Gas Station", "Travel Outlet"]
    property variant blue_base: ["University"]


    function define_color_locker(name){
        if(green_base.indexOf(name) > -1){
            return "green"
        }else if(chocolate_base.indexOf(name) > -1){
            return "chocolate"
        }else if(red_base.indexOf(name) > -1){
            return "red"
        }else if(blue_base.indexOf(name) > -1){
            return "blue"
        }else{
            return "white"
        }
    }

    Rectangle{
        id: base_background
        width: parent_rec.width
        height: parent_rec.height
        anchors.fill: parent
        radius: 10
        anchors.rightMargin: 0
        anchors.bottomMargin: 0
        anchors.leftMargin: 0
        anchors.topMargin: 0
        color: "white"
        border.width: 3
        border.color: "#00000000"
    }

    Rectangle{
        id: select_this_lock_base
        x: 0
        y: 0
        color: "darkred"
        width: parent.width
        height: 20
        Text{
            id: txt_select_this
            anchors.fill: parent
            text: qsTr("Select This Locker")
            font.pixelSize: 12
            style: Text.Normal
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            color: "white"
            font.family: "Microsoft YaHei"
        }
    }

    Rectangle{
        id: lock_name_base
        x: 110
        y: 20
        width: parent.width - lock_building_base.width
        height: 50
        color: {define_color_locker(lock_building)}

        Text{
            id: text_lock_name
            anchors.fill: parent
            text: lock_name
            font.italic: true
            textFormat: Text.PlainText
            color: (lock_name_base.color=="white") ? "black" : "white"
            wrapMode: Text.WordWrap
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 15
            font.family: "Microsoft YaHei"
            font.bold: true
            font.capitalization: Font.Capitalize
        }
    }

    Rectangle{
        id: lock_building_base
        color: "darkgrey"
        x: 0
        y: 20
        width: 110
        height: 50

        Text {
            id: text_lock_building
            height: parent.height
            anchors.fill: parent
            color: "black"
            text: lock_building
            anchors.rightMargin: 0
            anchors.leftMargin: 0
            anchors.topMargin: 0
            anchors.bottomMargin: 0
            font.italic: true
            wrapMode: Text.WordWrap
            textFormat: Text.PlainText
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: 12
            font.family: "Microsoft YaHei"
            horizontalAlignment: Text.AlignHCenter
            font.bold: true
        }
    }

    /*Rectangle{
        id: lock_operational_base
        color: (lock_operational.indexOf("24") > -1) ? "limegreen": "yellow"
        x: 158
        y: 60
        width: 217
        height: 46

        Text{
            id: text_lock_operational
            height: parent.height
            anchors.fill: parent
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            color: "black"
            font.family: "Microsoft YaHei"
            text: lock_operational
            font.pixelSize: (lock_operational.length > 33) ? 10 : 12
            anchors.leftMargin: 0
            wrapMode: Text.WordWrap
            font.italic: true
        }
    }*/

    Text{
        id: text_lock_addr
        x: 0
        y: 70
        width: 375
        height: 52
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        color: "black"
        font.family: "Microsoft YaHei"
        text: lock_addr_1 + " | " + lock_addr_2
        wrapMode: Text.WordWrap
        font.pixelSize: 10
    }

    Text {
        id: text_lock_operational
        x: 0
        y: 122
        width: 375
        height: 28
        color: "black"
        text: lock_operational
        font.family: "Microsoft YaHei"
        horizontalAlignment: Text.AlignHCenter
        wrapMode: Text.WordWrap
        textFormat: Text.PlainText
        font.pixelSize: (lock_operational.length > 33) ? 10 : 12
        verticalAlignment: Text.AlignVCenter
        font.italic: true
        font.capitalization: Font.Capitalize
    }
}
