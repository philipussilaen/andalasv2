function get_product(str){
    console.log("get_product result : ", str.length)
    var products = ""
    if(str=="promo"){
        products = [
                    {
                      "id": "272",
                      "sku": "PBSDIM00226",
                      "price": "25000",
                      "discount": "5000",
                      "total": "20000",
                      "name": "Samyang Mie Goreng Pedas",
                      "image": "https://shop.popbox.asia/assets/images/uploaded/10776416_2544e85e-30b1-4f78-978a-6c91b5555035.png",
                      "stock": "6",
                      "qrcode": "https://shop.popbox.asia/assets/images/qr/PBSDIM00226.svg"
                    },
                    {
                      "id": "433",
                      "sku": "PBSDIM00379",
                      "price": "80000",
                      "discount": "15000",
                      "total": "65000",
                      "name": "Granola - Original 300gr",
                      "image": "https://shop.popbox.asia/assets/images/uploaded/granova_original.jpg",
                      "stock": "4",
                      "qrcode": "https://shop.popbox.asia/assets/images/qr/PBSDIM00379.svg"
                    },
                    {
                      "id": "465",
                      "sku": "PBSDIM00406",
                      "price": "75000",
                      "discount": "10000",
                      "total": "65000",
                      "name": "Frame Foto (10x15.7cm)",
                      "image": "https://shop.popbox.asia/assets/images/uploaded/bingkai-coklat1.png",
                      "stock": "6",
                      "qrcode": "https://shop.popbox.asia/assets/images/qr/PBSDIM00406.svg"
                    },
                    {
                      "id": "170",
                      "sku": "PBSDIM00136",
                      "price": "96000",
                      "discount": "16000",
                      "total": "80000",
                      "name": "Vivan Charger Mobil  Hitam",
                      "image": "https://shop.popbox.asia/assets/images/uploaded/vivan_charger_mobil_hitam.png",
                      "stock": "4",
                      "qrcode": "https://shop.popbox.asia/assets/images/qr/PBSDIM00136.svg"
                    },
                    {
                      "id": "503",
                      "sku": "PBSDIMO0442",
                      "price": "200000",
                      "discount": "71000",
                      "total": "129000",
                      "name": "Lazy Bag - Biru",
                      "image": "https://shop.popbox.asia/assets/images/uploaded/biru1.png",
                      "stock": "9",
                      "qrcode": "https://shop.popbox.asia/assets/images/qr/PBSDIMO0442.svg"
                    }
                  ];
        return products;
    }else{
        return
    }
}
