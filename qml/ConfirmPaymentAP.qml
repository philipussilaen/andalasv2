import QtQuick 2.4
import QtQuick.Controls 1.2
import 'base_function.js' as FUNC
import 'door_price.js' as PRICE

BaseAP{
    id: confirmAP
    property var press: '0'
    watermark: true
    topPanelColor: 'BLUE'
    mainMode: false
    property var phone_number: ''
    property var selectedPayment: ''
    property int timer_value: 300
    property var duration: '8'
    property bool isExtending: false
    property var extendData: undefined
    property int show_timer_value: 5
    property int show_timer_value_nf: 5
    property int trial_count: 0
    property var paymentResult: undefined
    property var door_no: ''
    property var extend_pincode: ''
    property var extend_number: ''
    property var locker_name: ''
    property bool testingMode: false
    property var amount: '3000'
    signal sendManualTrigger(string str)


    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            press = '0';
            abc.counter = timer_value;
            my_timer.start();
            get_amount();
            if (isExtending==true){
                parse_data(extendData);
            }
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop();
        }
    }


    Component.onCompleted: {
        sendManualTrigger.connect(manual_trigger);
        root.mouth_number_result.connect(define_door_no);
        root.choose_mouth_result.connect(select_door_result);
        root.start_global_payment_emoney_result.connect(emoney_result);
        root.apexpress_result.connect(next_process);
        root.customer_take_express_result.connect(process_result);
        root.update_apexpress_result.connect(process_update);
        root.start_create_payment_result.connect(create_trans_global_result);
    }

    Component.onDestruction: {
        sendManualTrigger.disconnect(manual_trigger);
        root.choose_mouth_result.disconnect(select_door_result)
        root.mouth_number_result.disconnect(define_door_no);
        root.start_global_payment_emoney_result.disconnect(emoney_result);
        root.apexpress_result.disconnect(next_process);
        root.customer_take_express_result.disconnect(process_result);
        root.update_apexpress_result.disconnect(process_update);
        root.start_create_payment_result.disconnect(create_trans_global_result);
    }

    function get_amount(){
        if (testingMode==true){
            amount = '200';
        } else {
            amount = PRICE.popsafe_price;
        }
    }

    function create_trans_global_result(result){
        console.log("create_trans_global_result is ", result)
        if (result==""){
            return
        } else {
            var products = {'name': 'APEXPRESS_' + phone_number, 'qty': '1', 'total_amount': amount};
            my_stack_view.push(qr_payment_ap, {products: JSON.stringify(products), provider: "BNI-YAP",
            global_trans: result, phone_number: phone_number, duration: duration, paymentResult: result,
            extend_pincode: extend_pincode, isExtending: isExtending });
        }
    }

    function process_update(u){
        console.log('process_update');
        if (u=='Success'){
//            slot_handler.get_express_mouth_number();
            if (selectedPayment=='emoney'){
                slot_handler.start_push_data_settlement(phone_number +'-'+extend_number+'-'+ 'AP_EXPRESS_EXTEND');
            }
            slot_handler.start_video_capture("take_apexpress_" + extend_pincode);
            slot_handler.customer_take_express(extend_pincode+'||AP_EXPRESS');
        }
    }

    function process_result(text){
        console.log('process_result : ' + text)
        if (text=='Error'||text=='NotInput'){
            failure_notif.titleText = qsTr('Parcel Tidak Ditemukan');
            failure_notif.notifText = qsTr('Mohon Maaf, Silakan Masukkan Kode Pin Yang Benar.');
            failure_notif.open();
            return
        }
        var status = text.split('||')[0];
        var info = text.split('||')[1];
        var detail = JSON.parse(info);
        var d = JSON.parse(detail.transactionType)
//        if (status=='Overdue'){
//            my_stack_view.push(select_payment_ap, {phone_number: detail.phone_number, isExtending: true, extendData: info});
//        }
        if (status=='Success'){
            my_stack_view.push(take_parcel_ap, {door_no: d.door_no});
        }
    }

    function next_process(t){
        console.log('next_process', t);
        loadingPopUp.close();
        if (t=='ERROR'){
            notif_text.contentNotif = qsTr('Terjadi Kesalahan, Silakan Hubungi Layanan Pelanggan.');
            notif_text.successMode = false;
            notif_text.open();
            return
        }
        var info = JSON.parse(t);
        var param = {
            'paymentType': selectedPayment,
            'duration': duration,
            'amount': amount,
            'paymentStatus': info.result,
            'pin_code': info.pin_code,
            'date': info.date,
            'door_no': info.door_no,
            'result': paymentResult,
            'phone_number': phone_number,
            'express': info.express
        };
        if (selectedPayment=='emoney'){
            slot_handler.start_push_data_settlement(phone_number +'-'+ info.express +'-'+ 'AP_EXPRESS');
        }
        my_stack_view.push(open_door_ap, {summary: JSON.stringify(param)});
    }

    function select_door_result(r){
        console.log('select_door_result', r);
        switch (r){
        case 'NotMouth':
            notif_text.successMode = false;
            notif_text.contentNotif = qsTr('Mohon Maaf, Tidak Ada Loker Yang Tersedia Saat Ini.');
            notif_text.open();
            break;
        case 'Success':
//            slot_handler.get_express_mouth_number();
            slot_handler.start_store_apexpress(phone_number, duration);
            break;
        default:
            break;
        }
    }

    function define_door_no(d){
        console.log('define_door_no', d);
        door_no = d;
    }

    /*
          extendData = '{"id": "fc6852c09ba211e89941509a4ccea7f5", "mouth_id": "4028808359ff616b015a172842265752", "amount": 2000, "express_id": "fb4f280a9ba211e8be26509a4ccea7f5", "createTi
    me": 1533798495000, "transactionType": "{\"express\": \"APXID-9M2VX\", \"pin_code\": \"E3S9VC\", \"result\": \"SUCCESS\", \"door_no\": 44, \"phone_number\": \"085710157057\", \"date\": \"2018-08-09
     22:08:13\"}", "paymentType": "E3S9VC"}'
    */


    function parse_data(e){
        console.log('parse_date', e);
        var info = JSON.parse(e);
        extend_pincode = info.paymentType
        var detail = JSON.parse(info.transactionType)
        door_no = detail.door_no
        extend_number = detail.express
    }

    function emoney_result(result){
        console.log('emoney_payment_result', result);
        if(result == "UNFINISHED"){
            trial_count += 1;
            payment_page.close();
            nonfinished_notif.open();
            show_timer_payment.stop();
            show_timer_nf.restart();
            ok_button.enabled = false;
            trial_count_text.text = trial_count;
            time_nf.counter = show_timer_value_nf;
            if(trial_count >= 4){
                //slot_handler.start_reset_partial_transaction()
                my_timer.stop();
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }));
            }
        }else if(result == "WRONG-CARD" || result == "ERROR" || result == "FAILED"){
            payment_page.close();
            ok_button.enabled = false;
            failure_notif.notifText = qsTr('Pastikan Saldo Kartu eMoney Anda mencukupi');
            failure_notif.open();
            return
        }

        var obj = JSON.parse(result)
        if(obj.length <= 0){
            payment_page.close();
            ok_button.enabled = false;
            failure_notif.notifText = qsTr('Pastikan Saldo Kartu eMoney Anda mencukupi');
            failure_notif.open();
            return
        }else{
            loadingPopUp.popupText = qsTr('Memproses...');
            loadingPopUp.open();
            paymentResult = result;
            if (isExtending==false){
                if (testingMode==true){
                    slot_handler.start_choose_mouth_size("M","staff_store_express");
                } else {
                    slot_handler.start_choose_mouth_size("L","staff_store_express");
                }
            } else {
                slot_handler.start_update_apexpress(extend_pincode);
            }
        }
    }

    function manual_trigger(command){
        console.log('manual_trigger : ', command);
        if (command=='store_apexpress'){
            if (testingMode==true){
                slot_handler.start_choose_mouth_size("M","staff_store_express");
            } else {
                slot_handler.start_choose_mouth_size("L","staff_store_express");
            }
        }
    }


    BackButtonAP{
        id:back_button
        x:20
        y:20

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_timer.stop()
                my_stack_view.pop()
            }
        }
    }

    Rectangle{
        id: timer_set
        x:50
        y:50
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    Image {
        id: step_progress
        anchors.top: parent.top
        anchors.topMargin: 100
        anchors.horizontalCenter: parent.horizontalCenter
        source: "img/apservice/langkah/2.png"
    }

    Text {
        id: main_command_text
        width: 500
        height: 100
        color: "#323232"
        text: qsTr("Rincian Pesanan")
        anchors.left: parent.left
        anchors.leftMargin: 50
        anchors.top: parent.top
        anchors.topMargin: 200
        font.bold: true
        font.family: 'Microsoft YaHei'
        font.pixelSize: 50

    }

    Image {
        id: icon_confirm
        anchors.bottom: parent.bottom
        anchors.bottomMargin: -10
        anchors.right: parent.right
        anchors.rightMargin: 10
        source: "img/apservice/img/rincian_pesanan_biru.png"
    }

    Column{
        id: details_text
        anchors.verticalCenterOffset: 10
        anchors.left: parent.left
        anchors.leftMargin: 44
        anchors.verticalCenter: parent.verticalCenter
        spacing: 15
        RowTextAP{
            id: no_hp_text
            labelText: qsTr('No HP')
            contentText: phone_number
            colorContent: '#323232'
            contentBold: true
        }
        RowTextAP{
            id: amount_text
            labelText: qsTr('Jumlah')
            contentText: 'Rp.'+FUNC.insert_dot(amount)
            colorContent: '#009BE1'
            contentBold: true
        }
        RowTextAP{
            id: duration_text
            labelText: qsTr('Durasi')
            contentText: duration + ' Jam'
            colorContent: '#323232'
            contentBold: false
        }
        RowTextAP{
            id: payment_text
            labelText: qsTr('Bayar')
            contentText: selectedPayment.toLocaleUpperCase()
            colorContent: '#323232'
            contentBold: false
        }

    }

//    CheckBox{
//        id: check_tnc
//        x: 55
//        y: 535
//        checked: false
//    }
    Text{
        id: text_style_checkbox
        width: 600
        height: 100
        font.family: 'Microsoft YaHei'
        color: '#009BE1'
        text: qsTr('Dengan mengklik tombol Konfirmasi Anda telah setuju dengan Syarat dan Ketentuan yang berlaku.')
        anchors.verticalCenterOffset: 200
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.leftMargin: 50
        wrapMode: Text.WordWrap
        font.pixelSize: 23
        font.italic: true
//        MouseArea{
//            anchors.fill: parent
//            onClicked: {
//                check_tnc.checked = !check_tnc.checked;
//                press = '0';
//            }
//        }
    }

    NotifButtonAP{
        id: ok_button
        x: 365
        y: 644
//        buttonColor: (check_tnc.checked==true) ? 'BLUE' : 'GRAY'//'BLUE'/'GREEN'
//        enabled: check_tnc.checked
        buttonColor: 'BLUE'
        modeReverse: false
        buttonText: qsTr('KONFIRMASI')
        MouseArea{
            anchors.fill: parent
            onClicked: {
                if (press!='0') return
                press = '1'
                switch (selectedPayment){
                case 'emoney':
                    console.log('emoney selected');
                    payment_page.open();
                    show_timer_payment.restart();
                    break;
                case 'bni-yap':
                    console.log('bni-yap selected');
                    loadingPopUp.popupText = qsTr('Memproses...');
                    loadingPopUp.open();
                    var item = 'AP_EXPRESS @ ' + locker_name + ' : ' + door_no;
                    var customer = 'USER_APEXPRESS||'+phone_number+'||a@a.com';
                    slot_handler.start_create_payment_yap(amount, customer, item, locker_name)
                    break;
                default:
                    break;
                }
            }
        }
    }

    NotifButtonAP{
        id: cancel_button
        x: 50
        y: 644
//        buttonColor: (check_tnc.checked==true) ? 'BLUE' : 'GRAY'//'BLUE'/'GREEN'
//        enabled: check_tnc.checked
        buttonColor: 'BLUE'
        modeReverse: true
        buttonText: qsTr('BATAL')
        MouseArea{
            anchors.fill: parent
            onClicked: {
                my_timer.stop();
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }));
            }
        }
    }

    Notification{
        id: notif_text
        contentNotif: qsTr('Pastikan Nomor Telepon Anda adalah Benar.')
        successMode: false
    }

    LoadingPopUp{
        id: loadingPopUp
        z: 99
    }

    PaymentWindow{
        id:payment_page
//        visible:true
        useBaseOpacity: true
        globalOpacity: 1

        Text {
            id: text_payment_window
            x:334
            y:257
            width: 561
            height: 140
            text: qsTr("Tempelkan Kartu eMoney Anda di reader.")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            color:"#ab312e"
            textFormat: Text.PlainText
            font.pixelSize:30
            wrapMode: Text.WordWrap
        }

        Rectangle{
            QtObject{
                id:time_payment
                property int counter
                Component.onCompleted:{
                    time_payment.counter = show_timer_value
                }
            }
            Row{
                x:548
                y:399
                spacing:5
                Text {
                    id:show_time_payment_text
                    font.family:"Microsoft YaHei"
                    color:"#ab312e"
                    textFormat: Text.PlainText
                    font.pixelSize:25
                    wrapMode: Text.WordWrap
                }
                Text {
                    id:show_seconds_left_text
                    color: "#ab312e"
                    text: qsTr("detik")
                    font.family:"Microsoft YaHei"
                    font.pixelSize: 25
                }
            }

            Timer{
                id:show_timer_payment
                interval:1000
                repeat:true
                running:false
                triggeredOnStart:true
                onTriggered:{
                    show_time_payment_text.text = time_payment.counter
                    time_payment.counter -= 1
                    if(time_payment.counter == 2){
                        slot_handler.start_global_payment_emoney(amount);
                        //my_stack_view.push(on_develop_view)
                    }
                    if(time_payment.counter < 0){
                        show_timer_payment.stop()
                        show_time_payment_text.visible = false
                        show_seconds_left_text.visible = false
                        ok_button.enabled = true
                        abc.counter = timer_value
                    }
                }
            }
        }
    }

    PaymentWindow{
        id:nonfinished_notif
//        visible:true
        useBaseOpacity: true
        globalOpacity: 1


        Text {
            id: text_nonfisnihed_1
            x:130
            y:218
            width: 765
            height: 50
            text: qsTr("Transaksi Gagal")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            color:"#ab312e"
            textFormat: Text.PlainText
            font.pixelSize:40
            wrapMode: Text.WordWrap
        }

        Text {
            id: text_nonfisnihed_2
            x:340
            y:285
            width: 555
            height: 88
            text: qsTr("Tempelkan Kembali Kartu eMoney Anda.")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            color:"#ab312e"
            textFormat: Text.PlainText
            font.pixelSize:30
            wrapMode: Text.WordWrap
        }

        Rectangle{
            QtObject{
                id:time_nf
                property int counter
                Component.onCompleted:{
                    time_nf.counter = show_timer_value_nf
                }
            }
            Row{
                x:563
                y:391
                spacing:5
                Text {
                    id:show_time_nf
                    font.family:"Microsoft YaHei"
                    color:"#ab312e"
                    textFormat: Text.PlainText
                    font.pixelSize:40
                    wrapMode: Text.WordWrap
                }
                Text {
                    id: second_left_nf
                    color: "#ab312e"
                    text: qsTr("detik")
                    font.family:"Microsoft YaHei"
                    font.pixelSize: 25
                }
            }
            Row{
                x:862
                y:520
                width: 30
                height: 30
                spacing:5
                Text {
                    id:trial_count_text
                    width: 30
                    height: 30
                    font.family:"Microsoft YaHei"
                    color:"#ab312e"
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    textFormat: Text.PlainText
                    font.pixelSize:20
                    wrapMode: Text.WordWrap
                }
            }

            Timer{
                id:show_timer_nf
                interval:1000
                repeat:true
                running:false
                triggeredOnStart:true
                onTriggered:{
                    show_time_nf.text = time_nf.counter
                    time_nf.counter -= 1
                    if(time_nf.counter == 2){
                        slot_handler.start_global_payment_emoney(amount);
                        //my_stack_view.push(on_develop_view)
                    }
                    if(time_nf.counter < 0){
                        show_timer_nf.stop()
                        show_time_nf.visible = false
                        second_left_nf.visible = false
                    }
                }
            }
        }
    }

    // Change Below NOtif to Use New
    HideWindow{
        id:wrong_card_notif
//        visible:true

        Text {
            id: text_wc_1
            x: 93
            y:464
            width: 850
            height: 50
            text: qsTr("Transaction Failed")
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            textFormat: Text.PlainText
            font.pointSize:28
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        Text {
            id: text_wc_2
            x: 93
            y:520
            width: 850
            height: 50
            text: qsTr("Please use Mandiri e-Money prepaid card.")
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            textFormat: Text.PlainText
            font.pointSize:25
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        Image {
            id: img_wrong_card_notif
            x: 410
            y: 235
            width: 215
            height: 215
            fillMode: Image.PreserveAspectFit
            source: "img/otherservice/no_credit.png"
        }

        OverTimeButton{
            id:wrong_card_notif_back
            x:378
            y:589
            show_text:qsTr("Cancel")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 1) return true }))
                }
            }
        }
    }

    FailurePaymentAP{
        id: failure_notif
        titleText: qsTr('Pembayaran Gagal')
        notifText: qsTr('Mohon Maaf, Transaksi Anda gagal. Silakan Ulangi Pembayaran.')
    }

}
