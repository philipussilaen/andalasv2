import QtQuick 2.4
import QtQuick.Controls 1.2

Background{

    property var press: "0"
    property int timer_value: 30
    property var press_timer_button: 0

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            doorButton.show_image = "img/door/1.png"
            doorButton1.show_image = "img/door/1.png"
            press = "0"
            abc.counter = timer_value
            my_timer.restart()
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
        }
    }

    Timer{
        id:press_timer
        interval:3000
        repeat:true
        running:false
        triggeredOnStart:false
        onTriggered:{
            doorButton.enabled = true
            press_timer_button = 0
            press_timer.stop()
        }
    }

    DoorEvey{
        y:251
        x:263
    }

    DoorButton{
        id:doorButton
        y:640
        x:232
        show_text:qsTr("Open again")
        show_image:"img/door/1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press_timer_button == 0){
                    abc.counter = timer_value
                    my_timer.restart()
                    press_timer.start()
                    slot_handler.start_open_mouth_again()
                    doorButton.enabled = false
                    press_timer_button = 1
                }
            }
            onEntered:{
                doorButton.show_image = "img/door/2.png"
            }
            onExited:{
                doorButton.show_image = "img/door/1.png"
            }
        }
    }

    DoorButton{
        id: doorButton1
        y:640
        x:513
        show_text:qsTr("Confirm receipt")
        show_image:"img/door/1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_stack_view.push(pick_up_thanks_page)
            }
            onEntered:{
                doorButton1.show_image = "img/door/2.png"
            }
            onExited:{
                doorButton1.show_image = "img/door/1.png"
            }
        }
    }

    Row {
        x: 270
        y: 140
        layoutDirection: Qt.LeftToRight

        Text {
            id: box_open_tips_1
            color: "#FFFFFF"
            text: qsTr("Your locker is located at Box No ")
            font.family:"Microsoft YaHei"
            font.pixelSize: 30
        }

        Text {
            id:mouth_number
            color: "#FFFFFF"
            text: qsTr("X")
            font.family:"Microsoft YaHei"
            font.pixelSize: 30
        }
    }

    Column {
        x: 315
        y: 175
        spacing: 5

        Row {
            spacing: 10

            Text {
                color: "#FFFFFF"
                text: qsTr("The system will refresh in")
                font.family:"Microsoft YaHei"
                font.pixelSize: 24
            }

            Rectangle{
                width:39
                height:11
                y:10
                color:"transparent"
                QtObject{
                    id:abc
                    property int counter
                    Component.onCompleted:{
                        abc.counter = timer_value
                    }
                }

                Text{
                    id:countShow_test
                    x:183
                    y:500
                    anchors.centerIn:parent
                    color:"#FFFFFF"
                    font.family:"Microsoft YaHei"
                    font.pixelSize:24
                }

                Timer{
                    id:my_timer
                    interval:1000
                    repeat:true
                    running:true
                    triggeredOnStart:true
                    onTriggered:{
                        countShow_test.text = abc.counter
                        abc.counter -= 1
                        if(abc .counter < 0){
                            my_timer.stop()
                            my_stack_view.push(pick_up_thanks_page)
                        }
                    }
                }
            }

            Text {
                color: "#FFFFFF"
                text: qsTr("sec")
                font.family:"Microsoft YaHei"
                font.pixelSize: 24
            }
        }
    }

    FullWidthReminderText {
        x: 0
        y: 200
        remind_text:qsTr("Please ensure compartment door closure after deposit")
        remind_text_size:"24"
    }

    Component.onCompleted: {
        root.mouth_number_result.connect(show_text)
        slot_handler.get_express_mouth_number()
    }

    Component.onDestruction: {
        root.mouth_number_result.disconnect(show_text)
    }

    function show_text(t){
        mouth_number.text = t
    }

}
