import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id:other_input_memory_page

    property var show_text:""
    property int timer_value: 60
    property var press: "0"
    property var show_text_count:0

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            if(other_input_memory_page.show_text!=""){
                other_input_memory_page.show_text=""
                show_text_count = 0
            }
            //slot_handler.start_get_express_info_by_barcode()
            slot_handler.start_courier_scan_barcode()
            press = "0"
            abc.counter = timer_value
            my_timer.restart()
        }
        if(Stack.status==Stack.Deactivating){
            //slot_handler.stop_get_express_info_by_barcode()
            slot_handler.stop_courier_scan_barcode()
            my_timer.stop()
        }
    }

    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    BackButton{
            id:select_service_back_button
            x:20
            y:20
            show_text:qsTr("return")

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    my_timer.stop()
                    my_stack_view.pop()
                }
                onEntered:{
                    select_service_back_button.show_source = "img/bottondown/down.2.png"
                }
                onExited:{
                    select_service_back_button.show_source = "img/button/7.png"
                }
            }
    }

    Rectangle{
        id:main_page
         FullWidthReminderText{
            id:text
            y:125
            remind_text:qsTr("Scan or Enter AWB code")
            remind_text_size:"35"
        }

         Rectangle{
             x:220
             y:190
              width:620
              height:72
              color:"transparent"

          Image{
              width:620
              height:72
              source:"img/courier11/input1.png"
          }

          Text{
              id:textin
              y:10
              x:20
              font.family:"Microsoft YaHei"
              text:show_text
              color:"#FFFFFF"
              font.pixelSize:40
              }
          }

         Image{
             x: 412
             y: 280
             width:199
             height:64
             source:"img/button/barcode.png"
         }


        FullKeyboard{
            id:touch_keyboard
            x:29
            y:360
            property var count:show_text_count
            property var validate_code:""

            Component.onCompleted: {
                touch_keyboard.letter_button_clicked.connect(show_validate_code)
                touch_keyboard.function_button_clicked.connect(on_function_button_clicked)
            }

            function on_function_button_clicked(str){
                if(str == "ok"){
                    if(press != "0"){
                        return
                    }
                    press = "1"
                    if(other_input_memory_page.show_text == ""){
                        main_page.enabled = false
                        error_tips.open()
                    }
                    else{
                        count = 0
                        loading.open()
                        slot_handler.start_video_capture("other_service" + "_" + show_text)
                        slot_handler.start_get_express_info(show_text)
                        touch_keyboard.enabled = false
                        //my_stack_view.push(other_service_express_info_page)
                    }
                }

                if(str=="delete"){
                    if(count>=20){
                        count=19
                    }
                }
            }

            function show_validate_code(str){
                if (str == "" && count > 0){
                    if(count>=20){
                        count=20
                    }
                    count--
                    other_input_memory_page.show_text=other_input_memory_page.show_text.substring(0,count);
                }
                if (str != "" && count < 20){
                    count++
                }
                if (count>=20){
                    str=""
                }
                else{
                    other_input_memory_page.show_text += str
                }
                abc.counter = timer_value
                my_timer.restart()
            }
        }
    }

    Component.onCompleted: {
        root.start_get_express_info_result.connect(handle_text)
    }

    Component.onDestruction: {
        root.start_get_express_info_result.disconnect(handle_text)
    }

    function handle_text(result){
        loading.close()
        touch_keyboard.enabled = true
        if(result == ""){
            my_stack_view.push(other_search_fail_page)
        }
        else if(result == "ERROR"){
            my_stack_view.push(other_search_fail_page)
        }
        else if(result == "SUCCESS"){
            my_stack_view.push(other_service_express_info_page)
        }
        else if(result == "PAID"){
            my_stack_view.push(other_search_paid_page)
        }
    }

    LoadingView{
        id:loading
    }

        HideWindow{
        id:error_tips
        //visible: true

        Text {
            x: 112
            y:256
            width: 800
            height: 60
            wrapMode: Text.WordWrap
            text: qsTr("Oops...")
            font.family:"Microsoft YaHei"
            color:"#ffffff"
            textFormat: Text.PlainText
            font.pointSize:35
            horizontalAlignment: Text.AlignHCenter
        }

        Text {
            id: text_error
            x: 120
            y:300
            width: 809
            height: 250
            text: qsTr("We detected the order number is not entered yet. Please retry and enter the correct order number.")
            wrapMode: Text.WordWrap
            font.family:"Microsoft YaHei"
            color:"#FFFFFF"
            textFormat: Text.PlainText
            font.pointSize:30
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }


        OverTimeButton{
            id:ok_back
            x:373
            y:598
            show_text:qsTr("OK")
            show_x:15

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    press = "0"
                    abc.counter = timer_value
                    my_timer.restart()
                    other_input_memory_page.show_text == ""
                    show_text_count = 0
                    error_tips.close()
                    no_record.close()
                }
                onEntered:{
                    ok_back.show_source = "img/bottondown/down1.png"
                }
                onExited:{
                    ok_back.show_source = "img/button/7.png"
                }
            }
        }
    }
}
