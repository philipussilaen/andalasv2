import QtQuick 2.0

Item {
    id: root
    property int index: 0
    property MediaPlayer mediaPlayer
    property FolderListModel fm

    function setIndex(i)
    {
        index = i;
        console.log("setting index to: " + i);
        index %= fm.count;
        mediaPlayer.source = "file://" + fm.get(index, "filePath");
        console.log("setting source to: " + mediaPlayer.source);

    }

    function next()
    {
        setIndex(index + 1);
    }

    function previous()
    {
        setIndex(index + 1);
    }

    Connections {
        target: root.mediaPlayer
        onStopped: {
            if (root.mediaPlayer.status == MediaPlayer.EndOfMedia) {
                root.next();
                root.mediaPlayer.play();
            }
        }
    }
}
