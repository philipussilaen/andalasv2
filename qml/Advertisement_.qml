import QtQuick 2.4
import QtQuick.Controls 1.2
import Qt.labs.folderlistmodel 2.1

Rectangle{
    id:parent_root
    color: "black"
    property var img_path: "../advertisement/source/"
    property variant qml_pic: ["popsend.png", "bali_1.png", "bali_2.png", "bali_3.png", , "bali_4.png"]
    property string pic_source: ""
    property int num_pic: 0
    property variant list_pic: []

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            console.log(img_files.folder, img_files.count)
            get_img_files()
            //init_ad_images()
        }
        if(Stack.status==Stack.Deactivating){
            slider_timer.stop()
        }
    }

    Component.onCompleted: {
        get_img_files()
    }

    Component.onDestruction: {
        slider_timer.stop()
    }

    function init_ad_images(){
        loader.sourceComponent = loader.Null
        pic_source = img_path + qml_pic[0]
        loader.sourceComponent = component
        slider_timer.start()
    }

    function get_img_files(){
        list_pic = lv.model
        num_pic = 0
        if(list_pic.count>num_pic){
            var list = []
            for(var i=0; i<list_pic.count; i++){
                list.push(list_pic.get(i, "fileName"))
            }
            qml_pic = list
            console.log(qml_pic, qml_pic.length)
        }else{
            qml_pic = ["popsend.png", "bali_1.png", "bali_2.png", "bali_3.png", , "bali_4.png"]
        }
        init_ad_images()
    }

    ListView{
        id: lv
        visible: false

        FolderListModel{
            id: img_files
            folder: img_path
            nameFilters: ["*.png", "*.jpg", "*.jpeg"]
            showDirs: false
            showDotAndDotDot: false
            sortField: "Name"
        }

        model: img_files
    }

    Timer{
        id:slider_timer
        interval:5000
        repeat:true
        running:false
        triggeredOnStart:false
        onTriggered:{
            if(num_pic < qml_pic.length){
                num_pic += 1
                if(num_pic == qml_pic.length){
                    slider_timer.restart()
                    loader.sourceComponent = loader.Null
                    pic_source = img_path + qml_pic[0]
                    loader.sourceComponent = component
                    num_pic = 0
                }else{
                    slider_timer.restart()
                    loader.sourceComponent = loader.Null
                    pic_source = img_path + qml_pic[num_pic]
                    loader.sourceComponent = component
                }
            }
        }
    }

    Component {
        id: component
        Rectangle {
            AnimatedImage {
                id:ad_pic
                x: 0
                y: 0
                width: parent_root.width
                height: parent_root.height
                source: pic_source
                fillMode: Image.PreserveAspectFit
            }
        }
    }

    Loader { id: loader }

}

