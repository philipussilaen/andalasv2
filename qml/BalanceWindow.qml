import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    id:balance_float_window
    property var balance

    //默认的让这个页面隐藏起来
    visible: visible

    //模拟的一个遮罩
    Rectangle{
        x: 122
        y: 209
        width: 780
        height: 350
        color: "#ffffff"
        opacity: 0.8
        Image{
            id: money
            x:68
            y:104
            width:164
            height:143
            source: "img/otherservice/money_coin.png"
        }
        Text {
            id: title_balance
            x:252
            y:98
            width: 520
            height: 42
            text: qsTr("Your pre-paid balance is ")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            color:"#ab312e"
            textFormat: Text.PlainText
            font.pointSize:30
            wrapMode: Text.WordWrap
        }
        Text {
            id: balance_text
            x:252
            y:163
            width: 520
            height: 60
            text: balance
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family:"Microsoft YaHei"
            color:"#ab312e"
            textFormat: Text.PlainText
            font.pointSize:45
            wrapMode: Text.WordWrap
        }

    }
    function open(){
        balance_float_window.visible = true
    }
    function close(){
        balance_float_window.visible = false
    }
}
